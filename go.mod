module gitlab.com/internal-handbook/internal-handbook.gitlab.io

go 1.19

require (
	github.com/google/docsy v0.7.0 // indirect
	github.com/google/docsy/dependencies v0.7.0 // indirect
	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.1.2 // indirect
)
